class Person{
  constructor(name, age){
    this.name = name;
    this.age = age;
  }

  ShowInfo() {
    console.log(`Name: ${this.name}`);
    console.log(`Age: ${this.age}`);
  }
}

class Employer extends Person{
  constructor(name, age, salary){
    super(name, age);
    this.salary = salary
  }

  ShowSalary(){
    console.log(`Salary: ${this.salary}`);
  }
}

var person = new Employer("Dima", 22, 24000);

person.ShowInfo();
person.ShowSalary();
